var mongoose = require('mongoose')
var Schema = mongoose.Schema
var db = require('./index')

var TweetSchema = new mongoose.Schema({
	user: {type: Schema.Types.ObjectId, ref: 'User'},
	date: Date,			// date posted
	text: String,
	favorite: [{type: Schema.Types.ObjectId, ref: 'User'}],
	like: [{type: Schema.Types.ObjectId, ref: 'User'}]
})


TweetSchema.index({user: 1});
TweetSchema.index({date: -1});

module.exports = TweetSchema;