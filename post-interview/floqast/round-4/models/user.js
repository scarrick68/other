var mongoose = require('mongoose')
var Schema = mongoose.Schema
var db = require('./index')

var UserSchema = new mongoose.Schema({
	fn: {type: String, required: true},						// fn = first name, ln = last name
	ln: {type: String, required: true},
	handle: {type: String, unique: true, required: true},
	userEmail: {type: String, unique: true, required: true},
	userPhone: String,
	password: String,		// store the HASH of the pw. run the pw through the hash and then check match between hashes
	tweets: [{type: Schema.Types.ObjectId, ref: 'Tweet'}],					// object ids of listings posted by this user
	favorites: [{type: Schema.Types.ObjectId, ref: 'Tweet'}],
	likes: [{type: Schema.Types.ObjectId, ref: 'Tweet'}]
	date: Date,						// date the user signed up
})

// index these fields for quick searching of other users
// by a variety of fields. find user fast at sign-in too.
UserSchema.index({fn: 1});
UserSchema.index({ln: 1});
UserSchema.index({handle: 1});
UserSchema.index({userEmail: 1});
UserSchema.index({userPhone: 1});
// cursor can traverse an index in either direction. need to
// evaluate the real advtange, if any, of indexing in descending
// date order. Shouldn't affect functionality either way for the
// same reason, but may be confusing to developers building the
// API depending on expectations.
UserSchema.index({date: -1});

module.exports = UserSchema;