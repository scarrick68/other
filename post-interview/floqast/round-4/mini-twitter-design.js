
+-------------------------------------------------+
|                Some Tweet Text                  |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
|                                                 |
+-------------------------------------------------+
                                        +---------+
                                        |         |
                                        | Tweet!  |
                                        +---------+
  
// can post tweets
// can go to other user walls for tweets
  
// schema
/*
    Collections:
      - Tweet and User collections
    User Indexes:
      Single Field:
        - first name, last name, handle, email, phone # ()
          - fast searches for other users with any of their
            info you can remember
        - date in reverse chron order. still evaluating whether
          that has any effect vs. regular chron order, but index
          the dates. Probably be more useful for business side to
          eval users who signed up in diff time periods. date can
          be retrieved from obj id, but i'm not gonna sweat the
          extra memory. No extra step to get the date this way.
      Compound:
        - May help when searching for a user by multiple fields,
          but the system would need other logic to make that a
          smooth experience without the UI becoming clunky. Going to
          ignore this for now.

    Tweet Indexes:
      Single Field:
        - user object id and date
      Compound:
        - Doesn't seem to add additional value at this point. Might help
          getting specific user's tweets from certain date range, but
          if we go back in order most recent to least, I think we can just
          use the user index and skip().
*/

/*
  I'm debating whether or not we need an array of tweet object ids
  in the user collection at all. If I index on the user field in the
  tweet collection, I should be able to get the user's _id and use
  that as a param to query the tweet collection. I will leave it for
  now, and I can clear those fields later depending on how the API
  develops. 
*/

// API
// This file will be api.js in /routes
var db = require('../models')
var jwt = require('express-jwt')
var config = require('../config/config')

// client send post body something like this
{
  user: // the user's object id,
  tweet: 'this is my tweet text',
  favorite: [],
  like: []
}

// post new tweet. user must be signed in to access route.
router.post('/new-tweet', jwt({secret: config.secret}), function(req, res){
  var tweet = req.body
  tweet.date = new Date()
  // mongo obj
  tweet = new db.Tweet(tweet)
  tweet.save(function(err){
    if(err){
      return res.json({err: 'was unable to create new tweet'}) 
    }
    else{
      return res.json({msg: 'tweet created'})
    }
  })
})

// Get a user's wall of tweets. Need the user's _id. Can have some search
// box and click on a result returned from db. If their tweets show up
// somewhere else, we can include the _id in that data and have it sent
// when you click on their handle or something like that.
// Send date of last tweet when you hit the bottom of the page (or are close)
// and make a request for more tweets.

// check mongoose whether it's {field: {$lte...}} like in
// mongo shell or it's like some .lte() syntax.

router.get('/get-wall', function(req, res){
  var userId = req.query.userId
  var date = req.query.date || new Date()
  db.Tweet.find({user: userId, date: {$lte: date}})
          .sort({date: -1})
          .limit(50).exec(function(err, tweets){
              if(err){
                // pipe to log file or something
                console.err(err)
                return res.json({err: err})
              }
          })
})

// backend js files (maybe with function names)
/*
  - If api gets large, break it out into multiple files in a dir
    called api. /routes/api/api-files-go-her
  - Have folder for config files in the project root.
  - Have folder for models in the config root.
  - Put client side app files in the javascript folder created by
    express. Make new subdirs for controllers, services, directives
    or component equivalents for other client side frameworks.
  - Everything else, use express-generator default structure. If any
    piece starts to become too large, break it down into more subdirs.
*/