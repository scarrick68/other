var obj = {
            a:{
              a: {
                  b: "Leaf A-A-B"
                }
              },
              b: "Leaf B",
              c: {
                a: "Leaf C-A"
              },
              d: {
                a: "Leaf D-A"
              },
              E:{
                a: {
                  b: "Leaf E-A-B"
                }
            },
					}
  
// look at some level of the object
// Object.keys(currentObj)
// loop over them
// if current element is also an object, recurse
// else print
// typeof will tell me Object or String
function printLeaves(obj){
	var keys = Object.keys(obj)
  for(var i = 0; i < keys.length; i++){
    	if(typeof obj[keys[i]] === 'object'){
       	 printLeaves(obj[keys[i]])
      }
    	else{
	      	console.log(obj[keys[i]])
      }
  }
}

printLeaves(obj)

/*
    Debugging time: 5 minutes.
    Mistakes:
      - Used keys[i] instead of obj[keys[i]] in three places.
        1. Need to do typeof obj[keys[i]] instead of keys[i] to
        actually look at the value at that key and see if it's
        another object nested inside the current level. keys[i]
        will always give you a string and never recurse.
        2. printLeaves() needs to be passed the nested object itself
        for everything to work. I was just passing the key which never
        made it into the function anyway because of point 1.
        3. printing keys[i] will, again, just print the key
        and not the value which is the 'Leaf' that we wanted.
*/

// Original Implementation From The Interview

// function printLeaves(obj){
//   var keys = Object.keys(obj)
//   for(var i = 0; i < keys.length; i++){
//       if(typeof keys[i] === 'Object'){
//          printLeaves(keys[i])
//       }
//       else{
//           console.log(keys[i])
//       }
//   }
// }