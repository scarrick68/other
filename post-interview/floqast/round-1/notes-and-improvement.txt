// Analysis may be incomplete because I did not copy the sample code

Question: How would you code review this? (Piece of code was given)

My Answer:
	-Mostly revolved around refactoring for readability and formatting (partially because the copy paste doesn't look like it maintained the structure so that was probably moot relative to a real code review.).
	- I scanned the code and saw that it's doing some check on a csv or excel file.
	- I also questioned whether or not the code works because of a line like _.each(.map //other stuff here). I'm unfamiliar with the lodash / underscore API so that may be valid or it may be improper use of
	Array.map from the JS standard lib, so bringing that up and getting clarification would help.

Things I should have checked for:
	- Were variables defined close to where they were used? As Carlos pointed out, the error array was being modified within the function but declared somewhere else. This violates a principle of functional programming.
	- Should have asked about programming style, design patterns etc... that the organization uses. That would give a much clearer picture of what to look for and compare against. Similarly, is the company moving to new design patterns and is this code following the new structure?
	- Is the code easy to understand?
	- Are there tests to determine functionality of the code?
	- Are the tests comprehensive? May also need to review those depending on company's QA process.
	- Get clarity on what code is SUPPOSED to do before looking at what it is doing.

There are many other things to check for that require more in-depth knowledge of coding practices at a given company or specialized knowledge.

- security
- regulatory requirements
- fit into existing architecture with consistent design
- does it align with current needs? (reusuability, extensibility or YAGNI, you ain't gonna need it)