var trimmedString = {type: String, trim: true};

var UserSchema = new mongoose.Schema({
	fn: String,				// fn = first name, ln = last name
	ln: String,
	userEmail: String,
	userPhone: String,
	password: String,		// store the HASH of the pw. run the pw through the hash and then check match between hashes
	listings: Array,		// object ids of listings posted by this user
	date: Date,				// date the user signed up
})

UserSchema.index({fn: 1});
UserSchema.index({ln: 1});
UserSchema.index({userEmail: 1});
UserSchema.index({userPhone: 1});
UserSchema.index({date: -1});

module.exports = UserSchema;