var trimmedString = {type: String, trim: true};

var ApartmentListingSchema = new mongoose.Schema({
	user: ObjectId,
	// userEmail: String,
	// userPhone: String,
	price: Number,
	building: String,
	neighborhood: String,
	beds: Number,
	baths: Number,
	date: Date,
})

// Always get fast 
ApartmentListingSchema.index({user: 1});
// ApartmentListingSchema.index({userEmail: 1});
// ApartmentListingSchema.index({userPhone: 1});
ApartmentListingSchema.index({price: 1});
ApartmentListingSchema.index({neighborhood: 1});
ApartmentListingSchema.index({building: 1});
ApartmentListingSchema.index({bed: 1});
ApartmentListingSchema.index({baths: 1});
// order can be rearranged depending on user query prefs and patterns
ApartmentListingSchema.index({
								date: -1,
								price: 1,
								neighborhood: 1,
								building: 1,
								beds: 1,
								baths: 1
							});

module.exports = ApartmentListingSchema;