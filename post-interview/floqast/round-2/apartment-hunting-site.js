// Analysis may be incomplete because I did not
// copy the problem description or my code.

/*
	Problem:
		- Design a data model and API for an apartment
		hunting website
		- Users can search by price, neighborhood, 
		  building, (maybe also # bed and bath?)
		- Users can create new listings
		- Seems limited to one city atm. Otherwise need
		  more fields for state, country etc...
*/

/*
	Data Model Considerations:
		- What collections should the db have?
			- Apartment Listings and Users collections for sure.
			- Building and Neighborhood collections if they have
			  specific information, maybe like good_schools: Boolean,
			  high_rise: Boolean
				- If so, make Building and Neighborhood collections
				  and change those fields in listing schema to
				  store objectId of that building or
				  neighborhood. Use .populate() to
				  fill that data in when retrieving those records.
				  - Limiting returned results to 50 (or w/e) will limit
				    the effects of duplicate building / neighborhood info
				    we send back to the server and client for listings in
				    the same places. I'm imagining
				    a client side where listings are shown and then basic
				    building / neighborhood info is shown in the list of
				    apartments. You can click in for more detailed info.
				    Probably not worth additional effort to only send back
				    one copy of each building / neighborhood related to
				    the current search until later on in product lifecycle
				    and only if there are MANY users / requests and bandwidth
				    costs start to become more significant than the required
				    engineering effort.
		- Which fields to Index?
			- 	Apartment Listing Collection:
				-	Single Field Indexes:
					- Every field in apartment listing collection. Ascending order
					  is fine. Dates should probably be in descending order to get
					  N most recent by default. Can still be achieved with skip() and
					  limit() if Dates are indexed ascending or by using limit() with
					  .sort({$natural: -1}).
					  - Indexing on all fields in the apartment listing
						collection will allow for efficient searches on
						all fields independently of each other (maybe you just want prices
						and building / neighborhood don't matter. maybe anything in Echo Park
						is fine.), fulfilling the search condition of the problem.
				- 	Compound Indexes:
					- User queries should be analyzed for which fields they
					  search by most frequently and then build compound indexes
					  in order most frequently searched fields. Then the compound
					  index can be used for queries with those fields as well
					  as all prefix queries of that compound index.
			-	User Collection:
				-	Single Field Indexes:
					- Everything except array of that user's listings. Object ids will be in
					  that array and those are indexed by default in the listings collection.
				-	Compound Indexes:
					- Don't seem necessary here. User will usually be looked up by _id when
					  being referenced from listings collection, or email at sign-in. Those
					  fields are already indexed.
*/

/*
	API Considerations:
		-	Default listings shown to the user:
			- 	Show N most recently posted
			- 	Detect user location by IP Address and show
				N most recent for that location
				-	Users with VPN will probably get wrong info
					here, but probably not many people doing that
					so nvm it.
		- 	Post route to create new listings
			-	Require authorization to access this route.
		- 	Put route to update a listing, if spec requires
		- 	Get route to retrieve apartment listings based on provided query
			parameters
		-	Decide whether or not to populate user data in listing documents
			when getting those records or whether or not to make another route
			for that and request that information when a user clicks in to view
			a listing's details. Depends on UI design.
		-	Can users continue selecting new filters to narrow listings down?
			-	I think that would be an important feature. I am going
				to plan for it.
*/

// Client will send parameters to the server in query string
// req.query will provide them as an object which gets passed to mongo as the query object.
// Fields with multiple possibilities like neighborhood, building, bath should come
// from the client in arrays and the query will have a $in statement to match against
// all documents that match an element in each of the fields.

// Consider additional validation on server side. Would be good idea if API
// could be interacted with programmatically.

// Default query parameters will be arrays of distinct values for each field
// in the collection.

var jwt = require('express-jwt')

var defaults = {
	'neighborhood': [all neighborhoods],
	'building': [all buildings],
	'beds': [all beds],
	'baths': [all baths],
	'date': // default to today,
	'minPrice': 0,
	'maxPrice': Math.POSITIVE_INFINITY
}

// default object will have array of distinct neighborhoods,
// buildings, #'s of beds, #'s of baths
defaults = createDefaultObj(defaults)

// Use express-jwt to protect route. User must be auth'ed to create
// new apartment listing. pretty sure all routes that require auth
// can be specified in a config and express-jwt will take care of it.
router.post('/create-listing',
	jwt({secret: config.secret}),
	function(req, res){
		var listing = req.body
		listing = new db.Apartments(listing)
		listing.save(function(err){
			// handle the error
			// refresh default values in case new building or neighborhood etc. was added
			// that doesn't exist in the current default array.
			// send success indicator back to client
		})
})

// check mongoose whether it's {field: {$in...}} like in
// mongo shell or it's like some .in() syntax. same for $lte.
// clicking next page on client will send same query object
// with new date corresponding to oldest listing that is visible.
// assume 50 listings are sent back each time and they are all
// visible on one page. easy to change the number.

/*
	I like the query like this because then I can have a bunch
	of filters on the client and users can select / deselect
	multiple neighborhoods, building, # of beds and baths for more
	dynamic filtering options, while all coming through one API
	endpoint. It's reusable for all listing queries.
	Compound index(es) with a useful order will help us maintain
	efficient queries.
*/
router.get('/get-listings', function(req, res){
	var query = req.query || {}
	query = buildApartmentQuery(query)
	db.Apartments.find({neighborhood: {$in: query.neighborhood},
						 building: {$in: query.building},
						 beds: {$in: query.beds},
						 baths: {$in: query.baths},
						 date: {$lte: query.date},
						 minPrice: {$gte: minPrice},
						 maxPrice: {$lte: maxPrice}})
						.sort({date: -1})
						.limit(50)
						.exec((err, apartments) => {
							if(err){
								// log err
								// tell client something happened
								// client should display a msg to user
							}

						}),						 
})

// Assuming users will query for listings by neighborhood, building, bed, bath, price
// Every field will have a default. If no neighborhood is selected, it comes as []
// Replace it with Default.
// Same for other multi-selection fields.
function buildApartmentQuery(queryObj){
	if(queryObj.neighborhood.length === 0){
		queryObj.neighborhood = allNeighborhoods
	}
	if(queryObj.building.length === 0){
		queryObj.building = allBuildings
	}
	if(queryObj.beds.length === 0){
		queryObj.beds = allBeds
	}
	if(queryObj.baths.length === 0){
		queryObj.baths = allBaths
	}
	queryObj.date = queryObj.date || new Date()
	queryObj.minPrice = queryObj.minPrice || 0
	queryObj.maxPrice = queryObj.maxPrice || Math.POSITIVE_INFINITY
	return queryObj
}